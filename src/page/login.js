import React, { useState } from "react";
import { Input, Button } from "antd";
import hboApi from "../api/apiConfig";

import "../antd.css";

function Login({ history }) {
  const [account, setAccount] = useState("");
  const [password, setPassword] = useState("");

  const login = () => {
    hboApi
      .post(`AppService/Login`, {
        Account: account,
        Password: password
      })
      .then(function(res) {
        const { data: { accessToken, refreshToken } = {} } = res.data;
        if (accessToken && refreshToken) {
          localStorage.setItem("refresh_token", refreshToken);
          localStorage.setItem("access_token", accessToken);
          history.push("/");
        } else {
          localStorage.removeItem("refresh_token");
          localStorage.removeItem("access_token");
        }
      });
  };

  const style = {
    width: "300px",
    textAlign: "left"
  };

  return (
    <>
      <h2>Login</h2>
      <div style={style}>
        <Input
          placeholder="account"
          value={account}
          onChange={e => setAccount(e.target.value)}
        />
        <Input.Password
          placeholder="password"
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
        <p />
        <Button type="primary" onClick={login}>
          Login
        </Button>
      </div>
    </>
  );
}

export default Login;
