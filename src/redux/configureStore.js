import { combineReducers, createStore, applyMiddleware } from "redux";
import account from "./reducers/account";
import apiResponse from "./reducers/apiResponse";
import logger from "redux-logger";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import storage from 'redux-persist/lib/storage';

const isDevelopment = process.env.NODE_ENV === "development";
const rootReducer = combineReducers({
  account,
  apiResponse
});
const persistConfig = {
  key: "root",
  storage,
  whitelist: []
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
  const store = isDevelopment
    ? createStore(persistedReducer, applyMiddleware(thunk, logger))
    : createStore(persistedReducer, applyMiddleware(thunk));
  const persistor = persistStore(store);
  return {
    store,
    persistor
  };
};

