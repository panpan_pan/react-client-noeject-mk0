import React from "react";
import { render } from "react-dom";
import PageRouter from "./pageRouter";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/es/integration/react";
import configureStore from "./redux/configureStore";
import * as serviceWorker from "./serviceWorker";
import apiConfig from "./api/apiConfig";
import { apiResponseUpdate } from "./redux/actions/apiResponse";
import _ from "lodash";

const { persistor, store } = configureStore();
const { hboApi } = apiConfig;

// hboApi.interceptors.response.use(
//   function(response) {
//     const { dispatch } = store;
//     dispatch(apiResponseUpdate(response));
//     return response;
//   },
//   function(error) {
//     const { dispatch } = store;
//     dispatch(apiResponseUpdate(error));
//     return Promise.reject(error);
//   }
// );

hboApi.interceptors.request.use(
  config => {
    const access_token = _.get(
      store.getState(),
      "account.accountInfo.accessToken",
      null
    );
    if (access_token) config.headers["Access-Token"] = access_token;
    //Toast.loading("加载中...", 0);
    return config;
  },
  error => {
    //Toast.hide();
    Promise.reject(error);
  }
);

hboApi.interceptors.response.use(
  response => {
    const { dispatch } = store;
    dispatch(apiResponseUpdate(response));
    //Toast.hide();
    return response;
  },
  error => {
    const { dispatch } = store;
    const { code } = error;
    dispatch(apiResponseUpdate(error));
    //Toast.hide();
    //if (code === 401) dispatch(logout());
    return Promise.reject(error);
  }
);

render(
  <Provider store={store}>
    <PersistGate loading={null} onBeforeLift={() => null} persistor={persistor}>
      <PageRouter />
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
