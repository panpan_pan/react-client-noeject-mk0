import axios from "axios";
import _ from "lodash";

const API_PATH = 'AppService';
const API_LIST = {
  AddBoxMessagesInfo: `${API_PATH}/AddBoxMessagesInfo`,
  AddOrderInfo: `${API_PATH}/AddOrderInfo`,
  AddTraceOrderInfo: `${API_PATH}/AddTraceOrderInfo`,
  BankRecharge: `${API_PATH}/BankRecharge`,
  BindBankInfo: `${API_PATH}/BindBankInfo`,
  CancelOrder: `${API_PATH}/CancelOrder`,
  CancelRechargeMoney: `${API_PATH}/CancelRechargeMoney`,
  DailyAttendance: `${API_PATH}/DailyAttendance`,
  DeleteBank: `${API_PATH}/DeleteBank`,
  DrawingMoney: `${API_PATH}/DrawingMoney`,
  GetActivityStatus: `${API_PATH}/GetActivityStatus`,
  GetChartInfo: `${API_PATH}/GetChartInfo`,
  GetCustomerAddress: `${API_PATH}/GetCustomerAddress`,
  GetDailyAttendanceInfo: `${API_PATH}/GetDailyAttendanceInfo`,
  GetDailyBetRewards: `${API_PATH}/GetDailyBetRewards`,
  GetFristPayInfo: `${API_PATH}/GetFristPayInfo`,
  GetLotteryInfo: `${API_PATH}/GetLotteryInfo`,
  GetLotteryRather: `${API_PATH}/GetLotteryRather`,
  GetLotteryRatherByID: `${API_PATH}/GetLotteryRatherByID`,
  GetMessages: `${API_PATH}/GetMessages`,
  GetNotice: `${API_PATH}/GetNotice`,
  GetOrderInfo: `${API_PATH}/GetOrderInfo`,
  GetOrderList: `${API_PATH}/GetOrderList`,
  GetPaymentBankInfo: `${API_PATH}/GetPaymentBankInfo`,
  GetPlansDetailsInfo: `${API_PATH}/GetPlansDetailsInfo`,
  GetPresentNumber: `${API_PATH}/GetPresentNumber`,
  GetRealName: `${API_PATH}/GetRealName`,
  GetRewardAndBackPoint: `${API_PATH}/GetRewardAndBackPoint`,
  GetThirdPartyRecharge: `${API_PATH}/GetThirdPartyRecharge`,
  GetUserBankInfo: `${API_PATH}/GetUserBankInfo`,
  GetUserContent: `${API_PATH}/GetUserContent`,
  GetUserDayBetInfo: `${API_PATH}/GetUserDayBetInfo`,
  GetUserInfo: `${API_PATH}/GetUserInfo`,
  GetUserLotteryRecords: `${API_PATH}/GetUserLotteryRecords`,
  GetUserMoneyChange: `${API_PATH}/GetUserMoneyChange`,
  GetVersionInfo: `${API_PATH}/GetVersionInfo`,
  GetVersionInfo_v2: `${API_PATH}/GetVersionInfo_v2`,
  Login: `${API_PATH}/Login`,
  Logout: `${API_PATH}/Logout`,
  LotteryLobby: `${API_PATH}/LotteryLobby`,
  ReceiveRewards: `${API_PATH}/ReceiveRewards`,
  RefreshTerm: `${API_PATH}/RefreshTerm`,
  RefreshToken: `${API_PATH}/RefreshToken`,
  RefreshUserMoney: `${API_PATH}/RefreshUserMoney`,
  ThirdPartyRecharge: `${API_PATH}/ThirdPartyRecharge`,
  TransferMoney: `${API_PATH}/TransferMoney`,
  UpdateAccountInfo: `${API_PATH}/UpdateAccountInfo`,
  UpdateWithdrawPassword: `${API_PATH}/UpdateWithdrawPassword`,
  UpdateUserPwd: `${API_PATH}/UpdateUserPwd`,
  UserHeart: `${API_PATH}/UserHeart`,
  UserIsExist: `${API_PATH}/UserIsExist`,
};
const baseURL = "http://192.168.102.14:30001";
const hboApi = axios.create({
  baseURL
});

hboApi.interceptors.response.use(
  response => {
    const code = _.get(response, "data.code", null);
    if (code === 0) {
      return response.data;
    }
    return Promise.reject({
      ...response.data,
      name: "Response data code not zero"
    });
  },
  error => {
    return Promise.reject(error);
  }
);

function reqeustFactory(resource) {
  return function(data) {
    return hboApi.post(resource, data);
  };
}

const apiList = {};
Object.keys(API_LIST).forEach(key=>{
  apiList[key]=reqeustFactory(API_LIST[key]);
})

export default {
  hboApi,
  apiList
};
