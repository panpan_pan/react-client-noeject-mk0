import produce from "immer";

export default produce((state = { msg: {} }, action) => {
  switch (action.type) {
  case "APIRESPONSE_UPDATE":
    state.msg = action.msg;
    break;
  default:
  }
  return state;
});
