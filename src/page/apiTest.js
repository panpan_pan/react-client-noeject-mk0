import React, { useState } from "react";
import { connect } from "react-redux";
import apiConfig from "../api/apiConfig";
import apiList, {
  baseUrl
} from "../api/HBO_NewApp copy.postman_collection.json";
import ReactJson from "react-json-view";
import { Button, Input, Layout, Menu, Collapse, Icon, Divider } from "antd";
import "../antd.css";

const { Sider, Content } = Layout;
const { SubMenu } = Menu;
const { Panel } = Collapse;
const { hboApi } = apiConfig;

function blacklist(item) {
  const list = [];
  //const list = ["Login"];
  return !list.find(val => item.name === val);
}

function ApiButton({ url, rawModeData = "", name, setApiname }) {
  const postParams = rawModeData === "" ? {} : JSON.parse(rawModeData);
  const [postData, setPostData] = useState(postParams);

  const apiSend = () => {
    const apiResource = url.replace(baseUrl, "");
    setApiname(name);
    hboApi.post(apiResource, postData);
  };

  const inputChange = e =>
    setPostData({
      ...postData,
      [e.target.name]: e.target.value
    });

  return Object.keys(postData).length > 0 ? (
    <Collapse
      bordered={false}
      defaultActiveKey={[]}
      expandIconPosition="right"
      expandIcon={({ isActive }) => {
        return isActive ? (
          <Icon type="minus-square" />
        ) : (
          <Icon type="plus-square" />
        );
      }}
    >
      <Panel
        header={
          <>
            <Button type="primary" onClick={apiSend}>
              {name}
            </Button>
          </>
        }
        style={{
          borderBottom: "0px"
        }}
        showArrow={true}
        key="1"
      >
        <ul style={{ paddingLeft: "24px" }}>
          {Object.keys(postData).map((key, i) => (
            <li key={`${i}_li`} style={{ marginBottom: "2px" }}>
              <Input
                addonBefore={key}
                value={postData[key]}
                name={key}
                onChange={inputChange}
              />
            </li>
          ))}
        </ul>
      </Panel>
    </Collapse>
  ) : (
    <div
      style={{
        padding: "16px"
      }}
    >
      <Button type="primary" onClick={apiSend}>
        {name}
      </Button>
    </div>
  );
}

function findFolder(folderNode, folder, apiArr) {
  let result = false;
  folderNode.forEach(thisFolder => {
    if (thisFolder.id === folder.folder) {
      thisFolder.folders.push({
        name: folder.name,
        id: folder.id,
        folders: [],
        apis: folder.order.map(id => apiArr.find(el => el.id === id))
      });
      result = true;
    } else {
      findFolder(thisFolder.folders, folder, apiArr);
    }
  });
  return result;
}

function createFolderNode({
  folders: folderArr = [],
  requests: apiArr,
  order
}) {
  const folderNode = [];
  const _folderArr = [...folderArr];
  _folderArr.forEach((folder, i) => {
    if (folder.folder == null) {
      folderNode.push({
        name: folder.name,
        id: folder.id,
        folders: [],
        apis: folder.order.map(id => apiArr.find(el => el.id === id))
      });
      _folderArr[i] = null;
    }
  });

  let nextData = _folderArr.filter(node => node);
  while (nextData.length !== 0) {
    nextData.forEach((folder, i) => {
      const result = findFolder(folderNode, folder, apiArr);
      if (result) nextData[i] = null;
    });
    nextData = nextData.filter(node => node);
  }

  return {
    folders: folderNode,
    apis: order.map(id => apiArr.find(el => el.id === id))
  };
}

function nextItem(node, customProps) {
  if (node.folders.length !== 0) {
    return node.folders.map(node => (
      <SubMenu key={node.name} title={node.name}>
        {nextItem(node)}
        {node.apis.filter(blacklist).map((data, i) => {
          const props = {
            ...data,
            ...customProps
          };
          return <ApiButton {...props} key={`${i}_list`} />;
        })}
      </SubMenu>
    ));
  } else {
    return node.apis.filter(blacklist).map((data, i) => {
      const props = {
        ...data,
        ...customProps
      };
      return <ApiButton {...props} key={`${i}_list`} />;
    });
  }
}

function createItemOption({ folders, apis }, customProps) {
  return [
    ...folders.map(node => (
      <SubMenu key={node.name} title={node.name}>
        {nextItem(node, customProps)}
      </SubMenu>
    )),
    <div key="divider">
      <Divider />
    </div>,
    ...apis.filter(blacklist).map((data, i) => {
      const props = {
        ...data,
        ...customProps
      };
      return <ApiButton {...props} key={`${i}_list`} />;
    })
  ];
}

function ApiTest({ apiResponse: { msg = {} } }) {
  const [apiName, setApiname] = useState("");
  const style = {
    maxHeight: "calc(100vh - 64px)",
    overflow: "auto",
    padding: "0 20px"
  };
  const apiFolder = createFolderNode(apiList);
  const ItemOption = createItemOption(apiFolder, { setApiname });
  return (
    <>
      <Layout>
        <Sider width="400px">
          <Menu
            mode="inline"
            defaultSelectedKeys={["1"]}
            defaultOpenKeys={["sub1"]}
            style={style}
          >
            {ItemOption}
          </Menu>
        </Sider>
        <Content style={style}>
          <h3>{apiName}</h3>
          <ReactJson name={null} src={msg} />
        </Content>
      </Layout>
    </>
  );
}

export default connect(state => ({ apiResponse: state.apiResponse }))(ApiTest);
