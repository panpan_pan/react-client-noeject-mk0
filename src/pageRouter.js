import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Index from "./page/index";
import Login from "./page/login";
import ApiTest from "./page/apiTest";
import checkLogin from "./components/checkLogin";
import { Layout, Menu } from "antd";
import styled from 'styled-components'

const { Header } = Layout;

function Nav({ location: { pathname } }) {
  return (
    <Layout>
      <Header className="header">
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={[pathname]}
          style={{ lineHeight: "64px" }}
        >
          <Menu.Item key="/">
            <Link to="/">Home</Link>
          </Menu.Item>
          <Menu.Item key="/about/">
            <Link to="/about/">About</Link>
          </Menu.Item>
          <Menu.Item key="/apitest/">
            <Link to="/apitest/">Api test</Link>
          </Menu.Item>
        </Menu>
      </Header>
    </Layout>
  );
};

const StyledAbout = styled(About)`
  & h2{
    font-style: italic;
  }
  & p{
    width: 400px;
  }
`;

function About({className}) {
  return (
    <div className={className}>
      <h2>About</h2>
      <p>
        RJV is a React component for displaying and editing javascript arrays and JSON objects.
        This component provides a responsive interface for displaying arrays or JSON in a web browser.
        NPM offers a distribution of the source that&apos;s transpiled to ES5; so you can include this component with any web-based javascript application.
      </p>
    </div>
  );
}

const NoMatch = Login;
export default function PageRputer() {
  return (
    <Router>
      {/*Header*/}
      <Switch>
        <Route path="/" exact component={Nav} />
        <Route path="/about/" component={Nav} />
        <Route path="/apitest/" component={Nav} />
      </Switch>
      {/*Content*/}
      <Switch>
        <Route path="/login/" component={Login} />
        <Route path="/" exact render={checkLogin(Index)} />
        <Route path="/about/" render={checkLogin(StyledAbout)} />
        <Route path="/apitest/" render={checkLogin(ApiTest)} />
        <Route component={NoMatch} />
      </Switch>
    </Router>
  );
};
